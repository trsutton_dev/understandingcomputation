﻿namespace SmallStepSemanticsTest

open Swensen.Unquote
open Xunit
open SmallStepSemantics

module ExpressionTest =

    //#region Number Tests
        
    [<Fact>]
    let ``Number(n) as a string = 'n'``() =
        test <@ Number(5) |> Expression |> Ast.toString = "5" @>

    [<Fact>]
    let ``A Number is not reducible``() =
        test <@ Number(1) |> Expression |> Ast.isReducible = false @>

    //#endregion

    //#region Boolean Tests

    [<Fact>]
    let ``Boolean(bool) as a string = true|false``() =
        test <@ Boolean(true) |> Expression |> Ast.toString = "True" @>
        test <@ Boolean(false) |> Expression |> Ast.toString = "False" @>

    [<Fact>]
    let ``A Boolean is not reducible``() =
        test <@ Boolean(true) |> Expression |> Ast.isReducible = false @>

    //#endregion

    //#region Add Tests

    [<Fact>]
    let ``Add(l, r) as a string = 'l + r'``() =
        test <@ Add(Number 1, Number 2) |> Expression |> Ast.toString = "1 + 2" @>

    [<Fact>]
    let ``Add is reducible``() =
        test <@ Add(Number 1, Number 2) |> Expression |> Ast.isReducible = true @>

    [<Fact>]
    let ``1 + 2 -> 3``() =
        test <@ Add(Number 1, Number 2) |> Expression |> Machine.run <| Map.empty = (Expression(Number 3), Map.empty) @>

    [<Fact>]
    let ``(1 + 2) + 3 -> 6``() =
        test <@ Add(Add(Number 1, Number 2), Number(3)) |> Expression |> Machine.run <| Map.empty = (Expression(Number 6), Map.empty) @>

    [<Fact>]
    let ``1 + (2 + 3) -> 6``() =
        test <@ Add(Number 1, Add(Number 2, Number 3)) |> Expression |> Machine.run <| Map.empty = (Expression(Number 6), Map.empty) @>

    //#endregion

    //#region Subtract Tests

    [<Fact>]
    let ``Subtract(l, r) as a string = 'l - r'``() =
        test <@ Subtract(Number 1, Number 2) |> Expression |> Ast.toString = "1 - 2" @>

    [<Fact>]
    let ``Subtract is reducible``() =
        test <@ Subtract(Number 1, Number 2) |> Expression |> Ast.isReducible = true @>

    [<Fact>]
    let ``5 - 1 -> 4``() =
        test <@ Subtract(Number 5, Number 1) |> Expression |> Machine.run <| Map.empty = (Expression(Number 4), Map.empty) @>

    [<Fact>]
    let ``(100 - 98) - 2 -> 0``() =
        test <@ Subtract(Subtract(Number 100, Number 98), Number(2)) |> Expression |> Machine.run <| Map.empty = (Expression(Number 0), Map.empty) @>

    [<Fact>]
    let ``5 - (3 - 2) -> 4``() =
        test <@ Subtract(Number 5, Subtract(Number 3, Number 2)) |> Expression |> Machine.run <| Map.empty = (Expression(Number 4), Map.empty) @>

    //#endregion

    //#region Multiply Tests

    [<Fact>]
    let ``Multiply(l, r) as a string = 'l * r'``() =
        test <@ Multiply(Number 1, Number 2) |> Expression |> Ast.toString = "1 * 2" @>

    [<Fact>]
    let ``Multiply is reducible``() =
        test <@ Multiply(Number 1, Number 2) |> Expression |> Ast.isReducible = true @>

    [<Fact>]
    let ``4 * 5 -> 20``() =
        test <@ Multiply(Number 4, Number 5) |> Expression |> Machine.run <| Map.empty = (Expression(Number 20), Map.empty) @>

    [<Fact>]
    let ``(2 * 3) * 4 -> 24``() =
        test <@ Multiply(Multiply(Number 2, Number 3), Number(4)) |> Expression |> Machine.run <| Map.empty = (Expression(Number 24), Map.empty) @>

    [<Fact>]
    let ``2 * (3 * 4) -> 24``() =
        test <@ Multiply(Number 2, Multiply(Number 3, Number 4)) |> Expression |> Machine.run <| Map.empty = (Expression(Number 24), Map.empty) @>

    //#endregion

    //#region Divide Tests

    [<Fact>]
    let ``Divide(l, r) as a string = 'l / r'``() =
        test <@ Divide(Number 1, Number 2) |> Expression |> Ast.toString = "1 / 2" @>

    [<Fact>]
    let ``Divide is reducible``() =
        test <@ Divide(Number 1, Number 2) |> Expression |> Ast.isReducible = true @>

    [<Fact>]
    let ``20 / 5 -> 4``() =
        test <@ Divide(Number 20, Number 5) |> Expression |> Machine.run <| Map.empty = (Expression(Number 4), Map.empty) @>

    [<Fact>]
    let ``(20 / 2) / 2 -> 5``() =
        test <@ Divide(Divide(Number 20, Number 2), Number 2) |> Expression |> Machine.run <| Map.empty = (Expression(Number 5), Map.empty) @>

    [<Fact>]
    let ``20 / (10 * 2) -> 4``() =
        test <@ Divide(Number 20, Divide(Number 10, Number 2)) |> Expression |> Machine.run <| Map.empty = (Expression(Number 4), Map.empty) @>

    //#endregion
    
    //#region LessThan

    [<Fact>]
    let ``LessThan(x, y) as a string = 'x < y'``() =
        test <@ LessThan(Number 1, Number 2) |> Expression |> Ast.toString = "1 < 2" @>

    [<Fact>]
    let ``4 < 5 -> true``() =
        test <@ LessThan(Number 4, Number 5) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>
    
    [<Fact>]
    let ``5 < 4 -> false``() =
        test <@ LessThan(Number 5, Number 4) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean false), Map.empty) @>

    [<Fact>]
    let ``(5 + 4) < (7 + 3) -> true``() =
        test <@ LessThan(Add(Number 5, Number 4), Add(Number 7, Number 3)) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>

    //#endregion

    //#region LessThanOrEqualTo

    [<Fact>]
    let ``str(LessThan(x, y)) -> x <= y``() =
        test <@ LessThanOrEqual(Number 1, Number 2) |> Expression |> Ast.toString = "1 <= 2" @>

    [<Fact>]
    let ``4 <= 5 -> true``() =
        test <@ LessThanOrEqual(Number 4, Number 5) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>

    [<Fact>]
    let ``5 <= 5 -> true``() =
        test <@ LessThanOrEqual(Number 5, Number 5) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>

    [<Fact>]
    let ``6 <= 5 -> true``() =
        test <@ LessThanOrEqual(Number 6, Number 5) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean false), Map.empty) @>

    [<Fact>]
    let ``(5 + 4) <= (7 + 3) -> true``() =
        test <@ LessThanOrEqual(Add(Number 5, Number 4), Add(Number 7, Number 3)) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>

    //#endregion

    //#region GreaterThan

    [<Fact>]
    let ``GreaterThan(x, y) as a string = 'x > y'``() =
        test <@ GreaterThan(Number 1, Number 2) |> Expression |> Ast.toString = "1 > 2" @>

    [<Fact>]
    let ``5 > 4 -> true``() =
        test <@ GreaterThan(Number 5, Number 4) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>
    
    [<Fact>]
    let ``4 > 5 -> false``() =
        test <@ GreaterThan(Number 4, Number 5) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean false), Map.empty) @>

    [<Fact>]
    let ``(2 + 2) > (-1 + 1) -> true``() =
        test <@ GreaterThan(Add(Number 2, Number 2), Add(Number(-1), Number 1)) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>

    //#endregion

    //#region GreaterThanOrEqual

    [<Fact>]
    let ``str(GreaterThanOrEqualTo(x, y)) -> x >= y``() =
        test <@ LessThanOrEqual(Number 1, Number 2) |> Expression |> Ast.toString = "1 <= 2" @>

    [<Fact>]
    let ``5 >= 4 -> true``() =
        test <@ GreaterThanOrEqual(Number 5, Number 4) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>

    [<Fact>]
    let ``5 >= 5 -> true``() =
        test <@ GreaterThanOrEqual(Number 5, Number 5) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>

    [<Fact>]
    let ``5 >= 6 -> true``() =
        test <@ GreaterThanOrEqual(Number 5, Number 6) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean false), Map.empty) @>

    [<Fact>]
    let ``(5 + 4) >= (7 + 3) -> false``() =
        test <@ GreaterThanOrEqual(Add(Number 5, Number 4), Add(Number 7, Number 3)) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean false), Map.empty) @>

    //#endregion

    //#region Equal

    [<Fact>]
    let ``Equal(x, y) as a string = 'x == y'``() =
        test <@ Equal(Number 1, Number 2) |> Expression |> Ast.toString = "1 == 2" @>

    [<Fact>]
    let ``1000 == 1000 -> true``() =
        test <@ Equal(Number 1000, Number 1000) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>
    
    [<Fact>]
    let ``1000 == -1000 -> false``() =
        test <@ Equal(Number 1000, Number(-1000)) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean false), Map.empty) @>

    [<Fact>]
    let ``(6 + 4) == (7 + 3) -> true``() =
        test <@ Equal(Add(Number 6, Number 4), Add(Number 7, Number 3)) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>

    //#endregion

    //#region NotEqual

    [<Fact>]
    let ``NotEqual(x, y) as a string = 'x != y'``() =
        test <@ NotEqual(Number 1, Number 2) |> Expression |> Ast.toString = "1 != 2" @>

    [<Fact>]
    let ``1000 != 100 -> true``() =
        test <@ NotEqual(Number 1000, Number 100) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>
    
    [<Fact>]
    let ``1000 != 1000 -> false``() =
        test <@ NotEqual(Number 1000, Number 1000) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean false), Map.empty) @>

    [<Fact>]
    let ``(5 + 4) != (7 + 3) -> true``() =
        test <@ NotEqual(Add(Number 5, Number 4), Add(Number 7, Number 3)) |> Expression |> Machine.run <| Map.empty = (Expression(Boolean true), Map.empty) @>

    //#endregion

    //#region Variable

    [<Fact>]
    let ``Variable(name) as a string = 'name'``() =
        test <@ Variable "SomeVariable" |> Expression |> Ast.toString = "SomeVariable" @>

    [<Fact>]
    let ``When Env['y'] = 5, Variable(y) -> 5``() =
        let env = Map.empty.Add("y", Number 5)
        test <@ Variable "y" |> Expression |> Machine.run <| env = (Expression(Number 5), env) @>

    //#endregion

    //#region DoNothing

    [<Fact>]
    let ``The DoNothing statement prints as 'do-nothing'``() =
        test <@ DoNothing |> Statement |> Ast.toString = "do-nothing" @>

    [<Fact>]
    let ``The DoNothing statement is not reducible``() =
        test <@ DoNothing |> Statement |> Ast.isReducible = false @>

    //#endregion

    //#region Assign

    [<Fact>]
    let ``Assign(y, 5) as a string = 'y = 5'``() =
        let env = Map.empty
        test <@ Assign("y", Number 5) |> Statement |> Ast.toString = "y = 5" @>

    [<Fact>]
    let ``Assign(y, 5) -> Env[y] = 5``() =
        test <@ Assign("y", Number 5) |> Statement |> Machine.run <| Map.empty = (Statement(DoNothing), Map.empty.Add("y", Number 5)) @>

    //#endregion