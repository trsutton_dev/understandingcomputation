﻿// Learn more about F# at http://fsharp.net. See the 'F# Tutorial' project
// for more guidance on F# programming.

#load "Ast.fs"
open Simple.DenotationalSemantics

// Define your library scripting code here
let x = Ast.Expression.toFSharp (Add(Variable "x", Multiply(Number 123, Number 456)))

