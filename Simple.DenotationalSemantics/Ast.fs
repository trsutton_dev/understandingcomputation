﻿namespace Simple.DenotationalSemantics

open Microsoft.FSharp.Quotations

type Expression =
| Number of int
| Boolean of bool
| Variable of string
| Add of Expression * Expression
| Subtract of Expression * Expression
| Multiply of Expression * Expression
| Divide of Expression * Expression

type Statement =
| DoNothing
| Assign of string * Expression

type AST =
| Expression of Expression
| Statement of Statement

type Environment = Map<string, Expr>
type Environment2 = Map<string, obj>

module Env =
    let empty : Environment = Map.empty

    let create (entries: (string * Expr) list) = List.fold (fun acc (key, value) -> Map.add key value acc) Map.empty entries

    let merge (env1: Environment) (env2: Environment) = Map.fold (fun acc key value -> Map.add key value acc) env1 env2

    let mergeValue key value env = merge env (create [(key, value)])

module Env2 =
    let empty : Environment2 = Map.empty

    let create (entries: (string * obj) list) : Environment2 = List.fold (fun acc (key, value) -> Map.add key value acc) Map.empty entries

    let merge (env1: Environment2) (env2: Environment2) : Environment2 = Map.fold (fun acc key value -> Map.add key value acc) env1 env2

    let mergeValue key value env : Environment2 = merge env (create [(key, value)])

module Ast =
    module ExpressionTrees =
        module Expression =
            let rec toFSharp (expression: Expression) : (Environment -> Expr) =
                match expression with
                | Number(value) -> fun (e: Environment) -> <@@ value @@>
                | Boolean(value) -> fun (e: Environment) -> <@@ value @@>
                | Variable(name) -> fun (e: Environment) -> <@@ e.[name] @@>
                | Add(left, right) -> fun (e: Environment) -> <@@ %%(toFSharp left)(e) + %%(toFSharp right)(e) @@>
                | Subtract(left, right) -> fun (e: Environment) -> <@@ %%(toFSharp left)(e) - %%(toFSharp right)(e) @@>
                | Multiply(left, right) -> fun (e: Environment) -> <@@ %%(toFSharp left)(e) * %%(toFSharp right)(e) @@>    
                | Divide(left, right) -> fun (e: Environment) -> <@@ %%(toFSharp left)(e) / %%(toFSharp right)(e) @@>

        module Statement =
            let rec toFSharp (statement: Statement) : (Environment -> Expr<Environment>) =
                match statement with
                | DoNothing -> fun (e: Environment) -> <@ e @>
                | Assign(name, expression) -> 
                    fun (e: Environment) ->
                        let exp = Expression.toFSharp expression
                        let env = Env.mergeValue name (exp e) e
                        <@ env @>
    
    module Lambdas =
        module Expression =
            let rec toFSharp (expression: Expression) : (Environment2 -> obj) =
                match expression with
                | Number(value) -> fun e -> value :> obj
                | Boolean(value) -> fun e -> value :> obj
                | Variable(name) -> fun e -> e.[name]
                | Add(left, right) -> fun e ->
                    let l = (toFSharp left)(e) :?> int
                    let r = (toFSharp right)(e) :?> int
                    (l + r) :> obj
                | Subtract(left, right) -> fun e ->
                    let l = (toFSharp left)(e) :?> int
                    let r = (toFSharp right)(e) :?> int
                    (l - r) :> obj
                | Multiply(left, right) -> fun e ->
                    let l = (toFSharp left)(e) :?> int
                    let r = (toFSharp right)(e) :?> int
                    (l * r) :> obj
                | Divide(left, right) -> fun e ->
                    let l = (toFSharp left)(e) :?> int
                    let r = (toFSharp right)(e) :?> int
                    (l / r) :> obj
    
        module Statement =
            let rec toFSharp (statement: Statement) : (Environment2 -> Environment2) =
                match statement with
                | DoNothing -> fun e -> e
                | Assign(name, expression) ->
                    fun e ->
                        let exp = Expression.toFSharp expression
                        Env2.mergeValue name (exp e) e
