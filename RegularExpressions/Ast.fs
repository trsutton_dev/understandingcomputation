﻿namespace RegularExpressions

open FiniteAutomata

module private Helpers =
    let newState = fun () -> System.Guid.NewGuid()

    let transitionsToAcceptingState acceptStates (transition: FATransition<_,_>) = acceptStates |> Set.exists (fun s -> s = transition.NextState)

    let createFreeMovesFromAcceptingStatesTo acceptStates state =
        fun (transition: FATransition<_,_>) ->
            if transition |> transitionsToAcceptingState acceptStates then 
                [ transition; { State = transition.NextState; Symbol = None; NextState = state }; ]
            else
                [transition]

module Ast =
    open Helpers

    type AST =
        | Choose of AST * AST
        | Concatenate of AST * AST
        | Repeat of AST
        | Literal of char
        | Empty

    let precedence ast =
        match ast with
        | Choose(_, _) -> 0
        | Concatenate(_, _) -> 1
        | Repeat(_) -> 2
        | Literal(_) -> 3
        | Empty -> 3

    let rec toString ast : string =
        match ast with
        | Choose(first, second) as this         -> System.String.Join("|", (bracket first (precedence this)), (bracket second (precedence this)))
        | Concatenate(first, second) as this    -> System.String.Join("", (bracket first (precedence this)), (bracket second (precedence this)))
        | Repeat(pattern) as this               -> sprintf "%O*" (bracket pattern (precedence this))
        | Literal(c)                            -> sprintf "%O" c
        | Empty                                 -> "" 
    and bracket ast outerPrecedence =
        if precedence ast < outerPrecedence then sprintf "(%O)" (toString ast) else (toString ast)

    let rec toNFA ast =
        match ast with
        | Empty -> 
            let state = newState()
            NFA.create state (set [state]) []
        
        | Literal(c) ->
            let state1 = newState()
            let state2 = newState()
            NFA.create state1 (set [state2]) [ { State = state1; Symbol = Some c; NextState = state2} ]

        | Repeat(x) ->
            let nfa = x |> toNFA
            let newTransitions = nfa.Transitions |> List.collect (createFreeMovesFromAcceptingStatesTo nfa.AcceptStates nfa.StartState)
            let newStartTransition = { State = newState(); Symbol = None; NextState = nfa.StartState }
            NFA.create newStartTransition.State (nfa.AcceptStates |> Set.add newStartTransition.State) (newStartTransition :: newTransitions)
        
        | Concatenate(first, second) ->
            let (nfa1, nfa2) = ((first |> toNFA), (second |> toNFA))
            let newTransitions = nfa1.Transitions |> List.collect (createFreeMovesFromAcceptingStatesTo (nfa1.AcceptStates) (nfa2.StartState))
            { nfa1 with Transitions = newTransitions @ nfa2.Transitions; AcceptStates = nfa2.AcceptStates }

        | Choose(first, second) ->
            let insertFreeMoveToStartState startState newStartState =
                fun transition ->
                    if transition.State = startState then 
                        let freeMove = { State = newStartState; Symbol = None; NextState = newState() }
                        [ freeMove; { transition with State = freeMove.NextState } ]
                    else 
                        [transition]

            let (nfa1, nfa2) = ((first |> toNFA), (second |> toNFA))
            let newStartState = newState()

            let transitions1 = nfa1.Transitions |> List.collect (insertFreeMoveToStartState nfa1.StartState newStartState)
            let transitions2 = nfa2.Transitions |> List.collect (insertFreeMoveToStartState nfa2.StartState newStartState)
            
            NFA.create newStartState (set (nfa1.AcceptStates |> Set.union nfa2.AcceptStates)) (transitions1 @ transitions2)
            

    let matches input ast = (ast |> toNFA) |> NFA.accepts (input |> Seq.map (fun c -> Some c))