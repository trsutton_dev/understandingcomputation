﻿namespace RegularExpressions

type ParseResult<'a> =
    | Success of 'a * list<char>
    | Failure

type Parser<'a> = list<char> -> ParseResult<'a>

module Combinators =

    let Sat (predicate: char -> bool) =
        fun stream ->
            match stream with
            | x::xs when predicate x -> Success(x, xs)
            | _ -> Failure

    let Char (c: char) = Sat (fun x -> x = c)

    let Return (x: 'a) : Parser<'a> = fun stream -> Success(x, stream)

    let Bind (p: Parser<'a>) (f: 'a -> Parser<'b>) : Parser<'b> =
        fun stream ->
            match p stream with
            | Success(x, rest) -> (f x) rest
            | Failure -> Failure

    let (>>=) = Bind

    let Or (p1: Parser<'a>) (p2: Parser<'a>) : Parser<'a> =
        fun stream ->
            match p1 stream with
            | Failure -> p2 stream
            | result -> result

    let (<|>) = Or

    let EndOfInput : Parser<char> =
        fun stream ->
            match stream with
            | [] -> Success('\000', [])
            | _ -> Failure

    let LookAhead p =
        fun stream ->
            match p stream with
            | Success(x, _) -> Success(x, stream)
            | Failure -> Failure

    type ParserBuilder() =
        member x.Bind (p, f) = p >>= f
        member x.Return a = Return a
        member x.Zero() = EndOfInput

module Parser =
    open Combinators

    let parser = ParserBuilder()

    let rec Choose =
        parser {
            let! first = ConcatenateOrEmpty
            let! bar = Char '|'
            let! rest = Choose
            return Ast.Choose(first, rest)
        } <|> ConcatenateOrEmpty

    and ConcatenateOrEmpty = Concatenate <|> Empty
    
    and Concatenate =
        parser {
            let! first = Repeat
            let! rest = Concatenate
            return Ast.Concatenate(first, rest)
        } <|> Repeat
    
    and Empty = 
        parser { 
            let! empty = LookAhead (Sat (fun c -> not (System.Char.IsLetterOrDigit c)))
            return Ast.Empty 
        }
    
    and Repeat =
        parser {
            let! brackets = Brackets
            let! star = Char '*'
            return Ast.Repeat(brackets)
        } <|> Brackets
    
    and Brackets =
        parser {
            let! leftBrace = Char '('
            let! choose = Choose
            let! rightBrace = Char ')'
            return choose
        } <|> Literal
    
    and Literal =
        parser {
            let! literal = Sat System.Char.IsLetterOrDigit
            return Ast.Literal(literal)
        }

    let parse input = Choose input