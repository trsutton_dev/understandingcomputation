﻿namespace Simple.BigStepSemanticsTest

open Swensen.Unquote
open Xunit
open Simple.BigStepSemantics

module Tests =

    //#region Number

    [<Fact>]
    let ``The string reprsentation of a Number(n) -> 'n'``() =
        let number = Expression(Number 5)
        Assert.Equal("5", (number |> Ast.toString))

    [<Fact>]
    let ``A Number evaluates to itself``() =
        let result = Ast.Expression.evaluate (Number 5) (Env.empty)
        Assert.Equal(Number 5, result)

    //#endregion

    //#region Boolean

    [<Fact>]
    let ``The string representation of a Boolean(value) -> 'true|false'``() =
        let trueValue = Expression(Boolean true)
        let falseValue = Expression(Boolean false)
        Assert.Equal("True", (trueValue |> Ast.toString))
        Assert.Equal("False", (falseValue |> Ast.toString))

    [<Fact>]
    let ``A Boolean evaluates to itself``() =
        let trueResult = Ast.Expression.evaluate (Boolean true) (Env.empty)
        let falseResult = Ast.Expression.evaluate (Boolean false) (Env.empty)
        Assert.Equal(Boolean true, trueResult)
        Assert.Equal(Boolean false, falseResult)

    //#endregion

    //#region Variable

    [<Fact>]
    let ``The string representation of a Variable(name) -> 'name'``() =
        let name = "SomeVariable"
        let variable = Expression(Variable(name))
        Assert.Equal(name, variable |> Ast.toString)

    [<Fact>]
    let ``A Variable evaluates to it's value``() =
        let name = "x"
        let value = Number 5
        let env = Env.create [(name, value)]
        Assert.Equal(value, Ast.Expression.evaluate (Variable name) env)

    //#endregion

    //#region DoNothing

    [<Fact>]
    let ``The string representation of DoNothing -> 'do-nothing'``() =
        Assert.Equal("do-nothing", (Statement DoNothing) |> Ast.toString)

    [<Fact>]
    let ``Evaluating the DoNothing statement does not change the environment``() =
        let env = Env.create [("didChange", Boolean(false))]
        let newEnv = Ast.Statement.evaluate DoNothing env
        Assert.Equal<Environment>(env, newEnv)

    //#endregion

    //#region Assign

    [<Fact>]
    let ``The string representation of the Assign(name, expression) -> 'name = <evaluated expression>'``() =
        let assignStatement = Statement(Assign("x", Variable("y")))
        Assert.Equal("x = y", assignStatement |> Ast.toString)

    [<Fact>]
    let ``An Assign statement updates the environment with the results of the assignment``() =
        let env = Env.create [("x", Number 100); ("y", Number 0)]
        let assignStatement = Assign("y", Variable "x")
        let newEnv = Ast.Statement.evaluate assignStatement env
        Assert.NotEqual<Environment>(env, newEnv)
        Assert.Equal(Number 100, newEnv.["y"])

    //#endregion

    //#region Add

    [<Fact>]
    let ``The string representation of Add(l, r) -> 'l + r'``() =
        let add = Expression(Add(Number 1, Number 2))
        Assert.Equal("1 + 2", add |> Ast.toString)

    [<Fact>]
    let ``Returns the numerical result of adding the results of two numerical expressions``() =
        let add = Add(Add(Number 1, Number 2), Add(Number 3, Number 4))
        Assert.Equal(Number 10, Ast.Expression.evaluate add Env.empty)

    //#endregion

    //#region Subtract

    [<Fact>]
    let ``The string representation of Subtract(l, r) -> 'l - r'``() =
        let subtract = Expression(Subtract(Number 1, Number 2))
        Assert.Equal("1 - 2", subtract |> Ast.toString)

    [<Fact>]
    let ``Returns the numerical result of subtracting two numerical expressions``() =
        let subtract = Subtract(Subtract(Number 10, Number 5), Subtract(Number 3, Number 2))
        Assert.Equal(Number 4, Ast.Expression.evaluate subtract Env.empty)

    //#endregion

    //#region Multiply

    [<Fact>]
    let ``The string representation of Multiply(l, r) -> 'l * r'``() =
        let multiply = Expression(Multiply(Number 1, Number 2))
        Assert.Equal("1 * 2", multiply |> Ast.toString)

    [<Fact>]
    let ``Returns the numerical result of multiplying two numerical expressions``() =
        let multiply = Multiply(Multiply(Number 1, Number 2), Multiply(Number 3, Number 4))
        Assert.Equal(Number 24, Ast.Expression.evaluate multiply Env.empty)

    //#endregion

    //#region Divide

    [<Fact>]
    let ``The string representation of Divide(l, r) -> 'l / r'``() =
        let divide = Expression(Divide(Number 1, Number 2))
        Assert.Equal("1 / 2", divide |> Ast.toString)

    [<Fact>]
    let ``Returns the numerical result of dividing two numerical expressions``() =
        let divide = Divide(Multiply(Number 2, Number 5), Add(Number 1, Number 1))
        Assert.Equal(Number 5, Ast.Expression.evaluate divide Env.empty)

    //#endregion

    //#region Sequence
    
    [<Fact>]
    let ``The string representation of a Sequence statement is the aggregate of it's statements``() =
        let sequence = Statement(Sequence([Assign("x", Number 1); Assign("y", Number 2)]))
        Assert.Equal("x = 1\ny = 2", sequence |> Ast.toString)

    [<Fact>]
    let ``A Sequence evaluates each statement, threading the environment through each evaluation``() =
        // The following represents a number trick where given any number n,
        // if the following steps are taken, the result is always 13:
        //
        //    * Multiply n by 2
        //    * Add 15 to the result
        //    * Multiply the result by 3
        //    * Add 33 to the result
        //    * Divide the result by 6
        //    * Subtract n from the result
        let number = Number 12345
        let sequence = 
            Sequence([Assign("x", number);
                Assign("x", Multiply(Variable "x" , Number 2));
                Assign("x", Add(Variable "x", Number 15));
                Assign("x", Multiply(Variable "x", Number 3));
                Assign("x", Add(Variable "x", Number 33));
                Assign("x", Divide(Variable "x", Number 6));
                Assign("x", Subtract(Variable "x", number))])
        let env = Ast.Statement.evaluate sequence Env.empty
        Assert.Equal(Number 13, env.["x"])


    //#endregion

    //#region While

    [<Fact>]
    let ``A While expression executes the body if the condition is satisfied, threading the new environment from the body through each iteration of the loop``() =
        // The following represents a loop that calculates 5!:
        //
        // var n = 5
        // var i = 2
        // var result = 1
        //
        // while( i <= n )
        // {
        //     result = result * i
        //     i = i + 1
        // }
        let env = Env.create [("n", Number 5); ("i", Number 2); ("result", Number 1)]
        let factorial = While(LessThanOrEqualTo(Variable "i", Variable "n"), Sequence([Assign("result", Multiply(Variable "result", Variable "i")); Assign("i", Add(Variable "i", Number 1))]))
        
        let newEnv = Ast.Statement.evaluate factorial env
        
        Assert.Equal(Number 120, newEnv.["result"])

    //#endregion