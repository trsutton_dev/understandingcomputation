﻿namespace SmallStepSemantics

module Machine =

    let run(ast: AST) (environment: Environment) =
        let step ast env =
            Ast.reduce ast env
        let rec runInternal (ast, env) =
            if not (ast |> Ast.isReducible) then
                printfn "%s" (Ast.toString ast)
                ( ast, env )
            else
                printfn "%s" (Ast.toString ast)
                runInternal (step ast env)
        runInternal (ast, environment)
                

