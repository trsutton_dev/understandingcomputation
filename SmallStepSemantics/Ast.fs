﻿namespace SmallStepSemantics

type Expression =
| Number of int
| Boolean of bool
| Add of Expression * Expression
| Subtract of Expression * Expression
| Multiply of Expression * Expression
| Divide of Expression * Expression
| LessThan of Expression * Expression
| LessThanOrEqual of Expression * Expression
| GreaterThan of Expression * Expression
| GreaterThanOrEqual of Expression * Expression
| Equal of Expression * Expression
| NotEqual of Expression * Expression
| Variable of string

type Statement =
| DoNothing
| Assign of string * Expression

type AST =
| Expression of Expression
| Statement of Statement

type Environment = Map<string, Expression>

module Ast =

    let rec toString (ast: AST) =
        match ast with
        | Expression(Number(value))                     -> sprintf "%O" value
        | Expression(Boolean(value))                    -> sprintf "%O" value
        | Expression(Add(left, right))                  -> sprintf "%O + %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(Subtract(left, right))             -> sprintf "%O - %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(Multiply(left, right))             -> sprintf "%O * %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(Divide(left, right))               -> sprintf "%O / %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(LessThan(left, right))             -> sprintf "%O < %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(LessThanOrEqual(left, right))      -> sprintf "%O <= %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(GreaterThan(left, right))          -> sprintf "%O > %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(GreaterThanOrEqual(left, right))   -> sprintf "%O >= %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(Equal(left, right))                -> sprintf "%O == %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(NotEqual(left, right))             -> sprintf "%O != %O" (left |> Expression |> toString) (right |> Expression |> toString)
        | Expression(Variable(name))                    -> sprintf "%O" name
        | Statement(DoNothing)                          -> "do-nothing"
        | Statement(Assign(name, expression))           -> sprintf "%O = %O" name (expression |> Expression |> toString)

    let isReducible (ast: AST) =
        match ast with
        | Expression(Number(_))             -> false
        | Expression(Boolean(_))            -> false
        | Statement(DoNothing)              -> false
        | _                                 -> true

    let reduce (ast: AST) (environment: Environment) : AST * Environment =
        let expr (x: Expression * Environment) = fst x
        let merge (env1: Environment, env2: Environment) = Map.fold (fun acc key value -> Map.add key value acc) env1 env2
        let rec reduceExpression (expression: Expression) env =
            match expression with
            | Add(left, right) when Expression(left) |> isReducible -> ( Add(expr (reduceExpression left env), right), env )
            | Add(left, right) when Expression(right) |> isReducible -> ( Add(left, expr (reduceExpression right env)), env )
            | Add(Number(l), Number(r)) -> ( Number(l + r), env )

            | Subtract(left, right) when Expression(left) |> isReducible -> ( Subtract(expr (reduceExpression left env), right), env )
            | Subtract(left, right) when Expression(right) |> isReducible -> ( Subtract(left, expr (reduceExpression right env)), env )
            | Subtract(Number(l), Number(r)) -> ( Number(l - r), env )

            | Multiply(left, right) when Expression(left) |> isReducible -> ( Multiply(expr (reduceExpression left env), right), env )
            | Multiply(left, right) when Expression(right) |> isReducible -> ( Multiply(left, expr (reduceExpression right env)), env )
            | Multiply(Number(l), Number(r)) -> ( Number(l * r), env )

            | Divide(left, right) when Expression(left) |> isReducible -> ( Divide(expr (reduceExpression left env), right), env )
            | Divide(left, right) when Expression(right) |> isReducible -> ( Divide(left, expr (reduceExpression right env)), env )
            | Divide(Number(l), Number(r)) -> ( Number(l / r), env )

            | LessThan(left, right) when Expression(left) |> isReducible -> ( LessThan(expr (reduceExpression left env), right), env )
            | LessThan(left, right) when Expression(right) |> isReducible -> ( LessThan(left, expr (reduceExpression right env)), env )
            | LessThan(Number(l), Number(r)) -> ( Boolean(l < r), env )

            | LessThanOrEqual(left, right) when Expression(left) |> isReducible -> ( LessThanOrEqual(expr (reduceExpression left env), right), env )
            | LessThanOrEqual(left, right) when Expression(right) |> isReducible -> ( LessThanOrEqual(left, expr (reduceExpression right env)), env )
            | LessThanOrEqual(Number(l), Number(r)) -> ( Boolean(l <= r), env )

            | GreaterThan(left, right) when Expression(left) |> isReducible -> ( GreaterThan(expr (reduceExpression left env), right), env )
            | GreaterThan(left, right) when Expression(right) |> isReducible -> ( GreaterThan(left, expr (reduceExpression right env)), env )
            | GreaterThan(Number(l), Number(r)) -> ( Boolean(l > r), env )

            | GreaterThanOrEqual(left, right) when Expression(left) |> isReducible -> ( GreaterThanOrEqual(expr (reduceExpression left env), right), env )
            | GreaterThanOrEqual(left, right) when Expression(right) |> isReducible -> ( GreaterThanOrEqual(left, expr (reduceExpression right env)), env )
            | GreaterThanOrEqual(Number(l), Number(r)) -> ( Boolean(l >= r), env )

            | Equal(left, right) when Expression(left) |> isReducible -> ( Equal(expr (reduceExpression left env), right), env )
            | Equal(left, right) when Expression(right) |> isReducible -> ( Equal(left, expr(reduceExpression right env)), env )
            | Equal(Number(l), Number(r)) -> ( Boolean(l = r), env )

            | NotEqual(left, right) when Expression(left) |> isReducible -> ( NotEqual(expr (reduceExpression left env), right), env )
            | NotEqual(left, right) when Expression(right) |> isReducible -> ( NotEqual(left, expr(reduceExpression right env)), env )
            | NotEqual(Number(l), Number(r)) -> ( Boolean(l <> r), env )

            | Variable(name) -> ( env.[name], env )
        
            | _ -> failwithf "Failed to reduce expression '%O'" expression
        let reduceStatement statement env =
            match statement with
            | Assign(name, expression) when Expression(expression) |> isReducible -> ( Assign(name, expr (reduceExpression expression env)), env)
            | Assign(name, expression) -> ( DoNothing, merge(env, (Map.empty |> Map.add name expression)) )
            
            | _ -> failwithf "Failed to reduce statement '%O'" statement
            
        match ast with
        | Expression(expression) -> 
            let results = reduceExpression expression environment
            ( Expression(fst results), snd results )
        | Statement(statement) ->
            let results = reduceStatement statement environment
            ( Statement(fst results), snd results )