﻿namespace Simple.DenotationalSemanticsTest

open Swensen.Unquote
open Xunit
open Simple.DenotationalSemantics

module ExpressionTreeTests =

    [<Fact>]
    let ``Number(5) -> 5``() =
        let number = Ast.ExpressionTrees.Expression.toFSharp (Number 5)
        test <@ %%number(Env.empty) = 5 @>

    [<Fact>]
    let ``Boolean(true|false) -> true|false``() =
        let trueExpression = Ast.ExpressionTrees.Expression.toFSharp (Boolean true)
        let falseExpression = Ast.ExpressionTrees.Expression.toFSharp (Boolean false)
        test <@ %%trueExpression(Env.empty) = true @>
        test <@ %%falseExpression(Env.empty) = false @>

    [<Fact>]
    let ``Variable(x) -> env[x]``() =
        let env = Env.create [("x", Ast.ExpressionTrees.Expression.toFSharp (Number 3)(Env.empty))]
        let variable = Ast.ExpressionTrees.Expression.toFSharp (Variable "x")
        test <@ %%variable(env).Eval() = 3 @>

    [<Fact>]
    let ``Add(l, r) -> l + r``() =
        let add = Ast.ExpressionTrees.Expression.toFSharp (Add(Add(Number 1, Number 2), Add(Number 3, Number 4)))
        test <@ %%add(Env.empty) = 10 @>

    [<Fact>]
    let ``Subtract(l, r) -> l - r``() =
        let subtract = Ast.ExpressionTrees.Expression.toFSharp (Subtract(Add(Number 3, Number 4), Add(Number 1, Number 2)))
        test <@ %%subtract(Env.empty) = 4 @>

    [<Fact>]
    let ``Multiply(l, r) -> l * r``() =
        let multiply = Ast.ExpressionTrees.Expression.toFSharp (Multiply(Add(Number 1, Number 2), Add(Number 3, Number 4)))
        test <@ %%multiply(Env.empty) = 21 @>

    [<Fact>]
    let ``Divide(l, r) -> l / r``() =
        let divide = Ast.ExpressionTrees.Expression.toFSharp (Divide(Add(Number 20, Number 5), Subtract(Number 7, Number 2)))
        test <@ %%divide(Env.empty) = 5 @>

    [<Fact>]
    let ``DoNothing -> no changes to environment``() =
        let env = Env.create [("canRead", Ast.ExpressionTrees.Expression.toFSharp (Boolean false)(Env.empty))]
        let nothing = Ast.ExpressionTrees.Statement.toFSharp DoNothing
        test <@ %nothing(env) = env @>

    [<Fact>]
    let ``Assign(name, expression) -> env[name] = expression``() =
        let env = Env.empty
        let assign = Ast.ExpressionTrees.Statement.toFSharp (Assign("x", Add(Number 99, Number 1)))
        test <@ %assign(env) <> env @>
        test <@ (%assign(env)).["x"].Eval() = 100 @>