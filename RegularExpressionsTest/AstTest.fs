﻿namespace RegularExpressionsTest

open Xunit
open RegularExpressions
open RegularExpressions.Ast
open FiniteAutomata

module AstTests =
    let parseAST input =
        match Parser.parse (input |> Seq.toList) with
        | Success(ast, rest) -> ast
        | Failure -> failwithf "Failed to parse '%O'" input

    [<Fact>]
    let ``Correctly returns the precedence of each pattern type``() =
        Assert.Equal(0, precedence (Choose(Empty, Empty)))
        Assert.Equal(1, precedence (Concatenate(Empty, Empty)))
        Assert.Equal(2, precedence (Repeat(Empty)))
        Assert.Equal(3, precedence (Literal('a')))
        Assert.Equal(3, precedence Empty)

    [<Fact>]
    let ``Correctly returns a string representation of each AST``() =
        Assert.Equal("a|b",     Choose(Literal 'a', Literal 'b') |> toString)
        Assert.Equal("ab",      Concatenate(Literal 'a', Literal 'b') |> toString)
        Assert.Equal("(a|b)c",  Concatenate(Choose(Literal 'a', Literal 'b'), Literal('c')) |> toString)
        Assert.Equal("a*",      Repeat(Literal 'a') |> toString)
        Assert.Equal("(a|b)*",  Repeat(Choose(Literal 'a', Literal 'b')) |> toString)
        Assert.Equal("a",       Literal 'a' |> toString)
        Assert.Equal("",        Empty |> toString)

    [<Fact>]
    let ``The NFA for Empty should only match an empty string``() =
        Assert.True(Empty |> matches "")
        Assert.False(Empty |> matches "a")

    [<Fact>]
    let ``The NFA for Literal matches any character``() =
        ['a'; 'A'; '0'; '|'; '+'; '*'; '?'] |> List.iter (fun c -> Assert.True (Literal c |> matches (c.ToString())))
        [] |> List.iter (fun c -> Assert.False (Literal c |> matches (c.ToString())))

    [<Fact>]
    let ``The NFA for Repeat matches zero or more of a given AST``() =
        let ast = parseAST "a*"
        Assert.True(ast |> matches "")
        Assert.True(ast |> matches "a")
        Assert.True(ast |> matches "aaaa")
        Assert.False(ast |> matches "b")

        let ast2 = parseAST "(a|b)*"
        Assert.True(ast2 |> matches "")
        Assert.True(ast2 |> matches "a")
        Assert.True(ast2 |> matches "aaaa")
        Assert.True(ast2 |> matches "b")
        Assert.True(ast2 |> matches "abbbaab")
        Assert.False(ast2 |> matches "abcd")

    [<Fact>]
    let ``The NFA for Concatenate matches any sequence of ASTs``() =
        let ast = parseAST "ab"
        Assert.True(ast |> matches "ab")
        Assert.False(ast |> matches "a")
        Assert.False(ast |> matches "abc")

    [<Fact>]
    let ``The NFA for Choose matches either of two ASTs``() =
        let ast = parseAST "a|b"
        Assert.True(ast |> matches "a")
        Assert.True(ast |> matches "b")
        Assert.False(ast |> matches "c")
