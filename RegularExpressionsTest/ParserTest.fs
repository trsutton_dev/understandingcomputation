﻿namespace RegularExpressionsTest

open Xunit
open RegularExpressions

module Helpers =
    // A helper to convert to a list of chars
    let str (s: string) = s |> Seq.toList

module CombinatorTests =
    open Helpers
    open RegularExpressions.Combinators

    [<Fact>]
    let ``The Sat combinator returns Success if the input satisfies the given predicate; otherwise returns false``() =
        let input = str "abc"
        let matches = fun c -> c = input.[0]
        let doesNotMatch = fun c -> c = 'b'
        Assert.Equal(Success('a', str "bc"), (Sat matches) input)
        Assert.Equal(Failure, (Sat doesNotMatch) input)

    [<Fact>]
    let ``The Char combinator matches a given character``() =
        let input = str "abc"
        Assert.Equal(Success('a', str "bc"), (Char 'a') input)
        Assert.Equal(Failure, (Char 'b') input)
        
    [<Fact>]
    let ``The Return combinator returns the given value without consuming the input``() =
        let input = str "abc"
        Assert.Equal(Success('c', str "abc"), (Return 'c') input)

    [<Fact>]
    let ``The Bind combinator takes a parser and binding function to create a sequence of parsers``() =
        let input = str "abc"

        // Using the bind operator (>>=) here for succinctness 
        let parser = Char 'a' >>= fun a -> Char 'b' >>= fun b -> Char 'c' >>= fun c -> Return [a; b; c]
       
        Assert.Equal(Success(str "abc", []), parser input)  

    [<Fact>]
    let ``The Or(p1, p2) combinator takes two parsers and returns the result of p1 if successful; otherwise, returns the result of p2``() =
        let input = str "abc"
        let (p1, p2, p3) = (Char 'a', Char 'b', Char 'c')
        Assert.Equal(Success('a', str "bc"), (p1 <|> p2) input)
        Assert.Equal(Success('a', str "bc"), (p2 <|> p1) input)
        Assert.Equal(Failure, (p2 <|> p3) input)

    [<Fact>]
    let ``The EndOfInput combinator matches the end of the input``() =
        let input = str "a"
        let parser = Char 'a' >>= fun _ -> EndOfInput
        Assert.Equal(Success('\000', []), parser input)

    [<Fact>]
    let ``The LookAhead combinator applies the given parser but doesn't consume the input stream. Returns the result if successful``() =
        let input = str "abc"
        Assert.Equal(Success('a', str "abc"), (LookAhead (Char 'a')) input)
        Assert.Equal(Failure, (LookAhead (Char 'c')) input)


module ParserTests =
    open Helpers
    open RegularExpressions.Parser

    [<Fact>]
    let ``(a(|b))* -> Repeat(Concatenate(Literal('a'), Choose(Empty, Literal('b'))))``() =
        let input = str "(a(|b))*"
        let expected = Ast.Repeat(Ast.Concatenate(Ast.Literal('a'), Ast.Choose(Ast.Empty, Ast.Literal('b'))))
        match parse input with
        | Success(ast, rest) -> Assert.Equal(expected, ast)
        | Failure -> Assert.True(false)
