﻿namespace FiniteAutomata

type FATransition<'TState, 'TSymbol when 'TState : equality and 'TSymbol : equality> = 
    { 
        State : 'TState; 
        Symbol : Option<'TSymbol>; 
        NextState : 'TState 
    }


type DFAMachine<'TState, 'TSymbol when 'TState : equality and 'TSymbol : equality> = 
    { 
        StartState : 'TState; 
        AcceptStates : 'TState list; 
        Transitions : FATransition<'TState, 'TSymbol> list;
        CurrentState : 'TState  
    }


type NFAMachine<'TState, 'TSymbol when 'TState : comparison and 'TSymbol : equality> =
    {
        StartState : 'TState;
        AcceptStates : Set<'TState>;
        Transitions : FATransition<'TState, 'TSymbol> list;
        CurrentStates : Set<'TState>    
    }


module Transition =
    let appliesTo state symbol (transition: FATransition<_, _>) = (transition.State = state) && (transition.Symbol = symbol)

    let follow (transition: FATransition<_, _>) = transition.NextState

    let inspect (transition: FATransition<_, _>) = sprintf "#<FATransition %O  -- %O -->  %O>" (transition.State) (transition.Symbol |> Option.get) (transition.NextState)


module DFATransitionFunction =
    let transitionFor state symbol (transitions: FATransition<_, _> list) = 
        transitions 
        |> List.filter (fun transition -> transition.Symbol |> Option.isSome) // There are no free moves in a deterministic automaton, so filter transitions with no symbol 
        |> List.find (fun transition -> transition |> Transition.appliesTo state symbol)

    let nextState state symbol (transitions: FATransition<_, _> list) = transitions |> transitionFor state symbol |> Transition.follow


module DFA =
    let create startState acceptStates transitions = { StartState = startState; AcceptStates = acceptStates; Transitions = transitions; CurrentState = startState }

    let createFrom (dfa: DFAMachine<_, _>) = create (dfa.StartState) (dfa.AcceptStates) dfa.Transitions

    let accepting (dfa: DFAMachine<_, _>) = dfa.AcceptStates |> List.exists (fun state -> state = dfa.CurrentState)

    let readOne symbol (dfa: DFAMachine<_, _>) = { dfa with CurrentState = (dfa.Transitions |> DFATransitionFunction.nextState (dfa.CurrentState) symbol) }

    let readAll symbols (dfa: DFAMachine<_, _>) = Seq.fold (fun newDfa symbol -> newDfa |> readOne symbol) dfa symbols

    let accepts symbols (dfa: DFAMachine<_, _>) = createFrom dfa |> readAll symbols |> accepting


module NFATransitionFunction =
    let transitionsFor state symbol (transitions: FATransition<_, _> list) = 
        transitions |> List.choose (fun transition -> if (transition |> Transition.appliesTo state symbol) then Some(transition) else None)

    let nextStates states symbol (transitions: FATransition<_, _> list) =
        let nextStatesInternal states symbol transitions =
            let followTransitionsFor state symbol transitions = transitions |> transitionsFor state symbol |> List.map Transition.follow
            states |> Set.fold (fun acc state -> acc |> List.append <| (transitions |> followTransitionsFor state symbol)) [] |> set
        
        let rec followFreeMoves states transitions =
            let newStates = nextStatesInternal states None transitions
            if newStates.IsSubsetOf(states) then states else followFreeMoves (states + newStates) transitions
        
        nextStatesInternal (followFreeMoves states transitions) symbol transitions


module NFA =
    let create startState acceptStates transitions = { StartState = startState; AcceptStates = acceptStates; Transitions = transitions; CurrentStates = set [startState] }

    let create2 startState acceptStates transitions currentStates = { StartState = startState; AcceptStates = acceptStates; Transitions = transitions; CurrentStates = currentStates }

    let createFrom (nfa: NFAMachine<_, _>) = create (nfa.StartState) (nfa.AcceptStates) nfa.Transitions

    let accepting (nfa: NFAMachine<_, _>) = not (nfa.CurrentStates |> Set.intersect nfa.AcceptStates |> Set.isEmpty)

    let readOne symbol (nfa: NFAMachine<_, _>) = { nfa with CurrentStates = (nfa.Transitions |> NFATransitionFunction.nextStates (nfa.CurrentStates) symbol) }

    let readAll symbols (nfa: NFAMachine<_, _>) = Seq.fold (fun newNfa symbol -> newNfa |> readOne symbol) nfa symbols

    let accepts symbols (nfa: NFAMachine<_, _>) = createFrom nfa |> readAll symbols |> accepting

    module private Simulation =
        let nextState state symbol (nfa: NFAMachine<_,_>) =
            (create2 nfa.StartState nfa.AcceptStates nfa.Transitions state |> readOne symbol).CurrentStates

        let symbols (nfa: NFAMachine<_, _>) = nfa.Transitions |> List.choose (fun t -> Some(t.Symbol)) |> Set.ofList

        let transitionsFor state (nfa: NFAMachine<_,_>) = 
            (symbols nfa) |> Set.map (fun symbol -> { State = state; Symbol = symbol; NextState = (nextState state symbol nfa) })
            
        let rec discoverStatesAndTransitions states (nfa: NFAMachine<_, _>) =
            let transitions = states |> Set.toList |> List.collect (fun state -> (transitionsFor state nfa) |> Set.toList)
            let moreStates = transitions |> List.map Transition.follow |> set
            if moreStates.IsSubsetOf(states) then (states, transitions) else discoverStatesAndTransitions (states + moreStates) nfa

    let toDFA (nfa: NFAMachine<_,_>) =
        let startState = nfa.CurrentStates
        let (states, transitions) = nfa |> Simulation.discoverStatesAndTransitions (set [startState])
        let acceptStates = states |> Set.filter (fun state -> (create2 nfa.StartState nfa.AcceptStates nfa.Transitions state) |> accepting) |> Set.toList
        DFA.create startState acceptStates transitions
