﻿namespace FiniteAutomataTest

open Xunit
open FiniteAutomata

module ``The Transition module`` =

    [<Fact>]
    let ``.appliesTo determines if a rule applies to a given state and symbol``() =
        let (state, symbol, otherState, otherSymbol) = (1, Some 'a', 2, Some 'b')
        let rule = { FATransition.State = state; Symbol = symbol; NextState = otherState }
        Assert.True(rule |> Transition.appliesTo state symbol)
        Assert.False(rule |> Transition.appliesTo state otherSymbol)
        Assert.False(rule |> Transition.appliesTo otherState symbol)

    [<Fact>]
    let ``.follow follows the transition, yielding the next state``() =
        let nextState = 2
        let transition = { State = 1; Symbol = Some 'a'; NextState = nextState }
        Assert.Equal(nextState, transition |> Transition.follow)

    [<Fact>]
    let ``.inspect yields a string representation of the transition``() =
        let transition = { State = 1; Symbol = Some 'a'; NextState = 2 }
        Assert.Equal("#<FATransition 1  -- a -->  2>", transition |> Transition.inspect)


module ``The DFATransitionFunction module`` =

    [<Fact>]
    let ``.transitionFor gets the transition for which the given state and symbol apply``() =
        let (state, symbol, nextState) = (1, Some 'a', 2)
        let transition1 = { State = state; Symbol = symbol; NextState = nextState }
        let transition2 = { State = 2; Symbol = Some 'b'; NextState = 3 }
        Assert.Equal(transition1, [transition1; transition2] |> DFATransitionFunction.transitionFor state symbol)

    [<Fact>]
    let ``.transitionFor generates an error if the given state and symbol don't apply to any transitions i.e. the system must be deterministic``() =
        let transition = { State = 1; Symbol = Some 'a'; NextState = 2 }
        let action = fun () -> [transition] |> DFATransitionFunction.transitionFor 2 (Some 'a') |> ignore
        Assert.Throws<System.Collections.Generic.KeyNotFoundException>(action)

    [<Fact>]
    let ``.nextState finds the transition for which the given state and symbol apply, and from that, determines the next state``() =
        let (state, symbol, nextState) = (1, Some 'a', 2)
        let transition = { State = state; Symbol = symbol; NextState = nextState }
        Assert.Equal(nextState, [transition] |> DFATransitionFunction.nextState state symbol)

        
module ``The DFA module`` =
    
    [<Fact>]
    let ``.create creates a DFA machine given a starting state, accept states and a list of transitions``() =
        let startState = 1
        let acceptStates = [3; 4]
        let transitions = [ { State = 1; Symbol = Some 'a'; NextState = 2 } ]
        let expected = { StartState = startState; AcceptStates = acceptStates; Transitions = transitions; CurrentState = startState }
        Assert.Equal(expected, DFA.create startState acceptStates transitions)

    [<Fact>]
    let ``.accepting determines if the machine is in an accepting state``() =
        let startState = 1
        let acceptStates = [1; 4]
        let transitions = [ { State = 1; Symbol = Some 'a'; NextState = 2 } ]
        let dfa1 = DFA.create startState acceptStates transitions
        let dfa2 = DFA.create startState [2] transitions
        Assert.True(dfa1 |> DFA.accepting)
        Assert.False(dfa2 |> DFA.accepting)

    [<Fact>]
    let ``.readOne take a single symbol, consults the transition function, and changes state accordingly``() =
        let startState = 1
        let acceptStates = [2]
        let transitions = [ { State = 1; Symbol = Some 'a'; NextState = 2 }; { State = 2; Symbol = Some 'b'; NextState = 2 } ]
        let dfa = DFA.create startState acceptStates transitions
        let dfaAfterProcessing = dfa |> DFA.readOne (Some 'a')
        Assert.Equal(2, dfaAfterProcessing.CurrentState)

    [<Fact>]
    let ``.readAll take a sequence of symbols, consults the transition function, and changes state accordingly``() =
        let startState = 1
        let acceptStates = [3]
        let transitions = [ { State = 1; Symbol = Some 'a'; NextState = 2 }; { State = 2; Symbol = Some 'b'; NextState = 3 }; ]
        let dfa = DFA.create startState acceptStates transitions
        let dfaAfterProcessing = dfa |> DFA.readAll ("ab" |> Seq.map (fun c -> Some c))
        Assert.Equal(3, dfaAfterProcessing.CurrentState)
        

module ``The NFATransitionFunction module`` =
    let areEqual expected actual =
        let exp = set expected
        let act = set actual
        (act - exp) |> Set.isEmpty

    [<Fact>]
    let ``.transitionsFor gets the transitions for which the given state and symbol apply``() =
        let transition1 = { State = 1; Symbol = Some 'a'; NextState = 2 }
        let transition2 = { State = 1; Symbol = Some 'a'; NextState = 3 }
        let transition3 = { State = 2; Symbol = Some 'b'; NextState = 4 }
        let expected = [transition1; transition2]
        let actual = [transition1; transition2; transition3] |> NFATransitionFunction.transitionsFor 1 (Some 'a')
        Assert.True(areEqual expected actual)

    [<Fact>]
    let ``.nextStates finds the transitions for which the given states and symbol apply, and from that, determines the next possible states``() =
        let transitions =
            [
                { State = 1; Symbol = Some 'a'; NextState = 2 };
                { State = 1; Symbol = Some 'b'; NextState = 3 };
                { State = 2; Symbol = Some 'a'; NextState = 4 }
            ]
        let expected = [2; 4]
        let actual = transitions |> NFATransitionFunction.nextStates (set [1; 2]) (Some 'a')
        Assert.True(areEqual expected actual)

    [<Fact>]
    let ``.nextStates automatically traverses free states when finding next states``() =
        let transitions =
            [
                { State = 1; Symbol = None; NextState = 2 }
                { State = 1; Symbol = None; NextState = 3 }
                { State = 2; Symbol = Some 'a'; NextState = 4 }
                { State = 2; Symbol = Some 'b'; NextState = 2 }
                { State = 3; Symbol = Some 'a'; NextState = 5 }
                { State = 3; Symbol = Some 'b'; NextState = 3 }
            ]
        let expected = [4; 5]
        let actual = transitions |> NFATransitionFunction.nextStates (set [1]) (Some 'a')
        Assert.True(areEqual expected actual)

module ``The NFA module`` =
    let areEqual expected actual =
        let exp = set expected
        let act = set actual
        (act - exp) |> Set.isEmpty

    let str (s: string) = s |> Seq.toList |> List.map (fun c -> Some c)

    [<Fact>]
    let ``.create creates a NFA machine given a starting state, accept states and a list of transitions``() =
        let startState = 1
        let acceptStates = set [3; 4]
        let transitions = [ { State = 1; Symbol = Some 'a'; NextState = 2 } ]
        let expected = { StartState = startState; AcceptStates = acceptStates; Transitions = transitions; CurrentStates = set [startState] }
        Assert.Equal(expected, NFA.create startState acceptStates transitions)

    [<Fact>]
    let ``.createFrom creates a NFA machine given another machine``() =
        let otherNfa = NFA.create 1 (set [1; 2]) [ { State = 1; Symbol = Some 'a'; NextState = 2 }]
        let nfa = NFA.createFrom otherNfa
        Assert.Equal(otherNfa, nfa)

    [<Fact>]
    let ``.accepting determines if the machine is in an accepting state``() =
        let startState = 1
        let acceptStates = set [1; 4]
        let transitions = [ { State = 1; Symbol = Some 'a'; NextState = 2 } ]
        let nfa1 = NFA.create startState acceptStates transitions
        let nfa2 = NFA.create startState (set [2]) transitions
        Assert.True(nfa1 |> NFA.accepting)
        Assert.False(nfa2 |> NFA.accepting)

    [<Fact>]
    let ``.readOne take a single symbol, consults the transition function, and changes state accordingly``() =
        let startState = 1
        let acceptStates = set [2]
        let transitions = [ { State = 1; Symbol = Some 'a'; NextState = 1 }; { State = 1; Symbol = Some 'b'; NextState = 1 }; { State = 1; Symbol = Some 'b'; NextState = 2 } ]
        let nfa = NFA.create startState acceptStates transitions
        let nfaAfterProcessing = nfa |> NFA.readOne (Some 'b')
        Assert.True(areEqual ([1; 2]) (nfaAfterProcessing.CurrentStates))

    [<Fact>]
    let ``.readAll take a sequence of symbols, consults the transition function, and changes state accordingly``() =
        let startState = 1
        let acceptStates = set [4]
        let transitions = 
            [ 
                { State = 1; Symbol = Some 'a'; NextState = 1 }; 
                { State = 1; Symbol = Some 'b'; NextState = 1 };
                { State = 1; Symbol = Some 'b'; NextState = 2 };
                { State = 2; Symbol = Some 'a'; NextState = 3 };
                { State = 2; Symbol = Some 'b'; NextState = 3 };
                { State = 3; Symbol = Some 'a'; NextState = 4 };
                { State = 3; Symbol = Some 'b'; NextState = 4 };
            ]

        let nfa1 = NFA.create startState acceptStates transitions
        let nfa1AfterProcessing = nfa1 |> NFA.readAll ("aaaaaa" |> Seq.map (fun c -> Some c))
        Assert.False(nfa1AfterProcessing |> NFA.accepting)

        let nfa2 = NFA.create startState acceptStates transitions
        let nfa2AfterProcessing = nfa2 |> NFA.readAll ("aaabaa" |> Seq.map (fun c -> Some c))
        Assert.True(nfa2AfterProcessing |> NFA.accepting)

    [<Fact>]
    let ``.accepts creats a new NFA machine and reads all of the provided symbols to determine if they leave the machine in an accepting state``() =
        let startState = 1
        let acceptStates = set [4]
        let transitions = 
            [ 
                { State = 1; Symbol = Some 'a'; NextState = 1 }; { State = 1; Symbol = Some 'b'; NextState = 1 };
                { State = 1; Symbol = Some 'b'; NextState = 2 }; { State = 2; Symbol = Some 'a'; NextState = 3 };
                { State = 2; Symbol = Some 'b'; NextState = 3 }; { State = 3; Symbol = Some 'a'; NextState = 4 };
                { State = 3; Symbol = Some 'b'; NextState = 4 };
            ]

        let nfa = NFA.create startState acceptStates transitions
        
        Assert.False(nfa |> NFA.accepts ("aaaaaa" |> Seq.map (fun c -> Some c)))
        Assert.True(nfa |> NFA.accepts ("aaabaa" |> Seq.map (fun c -> Some c))) 

    [<Fact>]
    let ``Automatically traverses free moves when processing symbols``() =
        let startState = 1
        let acceptStates = set [2; 4]
        let transitions =
            [
                { State = 1; Symbol = None; NextState = 2 }
                { State = 1; Symbol = None; NextState = 4 }
                { State = 2; Symbol = Some 'a'; NextState = 3 }
                { State = 3; Symbol = Some 'a'; NextState = 2 }
                { State = 4; Symbol = Some 'a'; NextState = 5 }
                { State = 5; Symbol = Some 'a'; NextState = 6 }
                { State = 6; Symbol = Some 'a'; NextState = 4 }
            ]

        for i in 1 .. 10 do
            let nfa = NFA.create startState acceptStates transitions
            let symbol = seq { 1 .. i } |> Seq.map (fun _ -> Some 'a')
            let nfaAfterProcessing = nfa |> NFA.readAll symbol
            Assert.Equal((i % 2 = 0) || (i % 3 = 0), nfaAfterProcessing |> NFA.accepting)

    [<Fact>]
    let ``.toDFA converts a NFA to a DFA``() =
        let startState = 1
        let acceptStates = set [2; 4]
        let transitions =
            [
                { State = 1; Symbol = None; NextState = 2 }
                { State = 1; Symbol = None; NextState = 4 }
                { State = 2; Symbol = Some 'a'; NextState = 3 }
                { State = 3; Symbol = Some 'a'; NextState = 2 }
                { State = 4; Symbol = Some 'a'; NextState = 5 }
                { State = 5; Symbol = Some 'a'; NextState = 6 }
                { State = 6; Symbol = Some 'a'; NextState = 4 }
            ]
        
        let nfa = NFA.create startState acceptStates transitions
        Assert.True(nfa |> NFA.accepts (str "aa"))
        Assert.True(nfa |> NFA.accepts (str "aaa"))
        Assert.False(nfa |> NFA.accepts (str "aaaaa"))

        let dfa = nfa |> NFA.toDFA
        Assert.True(dfa |> DFA.accepts (str "aa"))
        Assert.True(dfa |> DFA.accepts (str "aaa"))
        Assert.False(dfa |> DFA.accepts (str "aaaaa"))

