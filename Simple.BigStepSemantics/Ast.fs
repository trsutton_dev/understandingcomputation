﻿namespace Simple.BigStepSemantics

type Expression =
| Number of int
| Boolean of bool
| Variable of string
| Add of Expression * Expression
| Subtract of Expression * Expression
| Multiply of Expression * Expression
| Divide of Expression * Expression
| LessThanOrEqualTo of Expression * Expression

type Statement =
| DoNothing
| Assign of string * Expression
| Sequence of Statement list
| While of Expression * Statement

type AST =
| Expression of Expression
| Statement of Statement

type Environment = Map<string, Expression>

module Env =
    let empty : Environment = Map.empty

    let create (entries: (string * Expression) list) = List.fold (fun acc (key, value) -> Map.add key value acc) Map.empty entries

    let merge (env1: Environment) (env2: Environment) = Map.fold (fun acc key value -> Map.add key value acc) env1 env2

    let mergeValue key value env = merge env (create [(key, value)])

module Ast =

    let rec toString (ast: AST) =
        match ast with
        | Expression(Number(value)) -> sprintf "%O" value
        | Expression(Boolean(value)) -> sprintf "%O" value
        | Expression(Variable(name)) -> sprintf "%O" name
        | Expression(Add(left, right)) -> sprintf "%O + %O" (Expression(left) |> toString) (Expression(right) |> toString)
        | Expression(Subtract(left, right)) -> sprintf "%O - %O" (Expression(left) |> toString) (Expression(right) |> toString)
        | Expression(Multiply(left, right)) -> sprintf "%O * %O" (Expression(left) |> toString) (Expression(right) |> toString)
        | Expression(Divide(left, right)) -> sprintf "%O / %O" (Expression(left) |> toString) (Expression(right) |> toString)
        | Expression(LessThanOrEqualTo(left, right)) -> sprintf "%O <= %O" (Expression(left) |> toString) (Expression(right) |> toString)
        
        | Statement(DoNothing) -> "do-nothing"
        | Statement(Assign(name, expression)) -> sprintf "%O = %O" name (Expression(expression) |> toString)
        | Statement(Sequence(statements)) -> statements |> List.map (fun stmt -> Statement(stmt) |> toString) |> String.concat "\n"
        | Statement(While(condition, body)) -> sprintf "while ( %O ) { %O }" (Expression(condition) |> toString) (Statement(body) |> toString)

    module Expression =
        let rec evaluate (expression: Expression) (env: Environment) =
            match expression with
            | Number(_) as n -> n
            | Boolean(_) as b -> b
            | Variable(name) -> env.[name]
            | Add(Number(l), Number(r)) -> Number(l + r)
            | Add(left, right) -> evaluate (Add(evaluate left env, evaluate right env)) env
            | Subtract(Number(l), Number(r)) -> Number(l - r)
            | Subtract(left, right) -> evaluate (Subtract(evaluate left env, evaluate right env)) env
            | Multiply(Number(l), Number(r)) -> Number(l * r)
            | Multiply(left, right) -> evaluate (Multiply(evaluate left env, evaluate right env)) env
            | Divide(Number(l), Number(r)) -> Number(l / r)
            | Divide(left, right) -> evaluate (Divide(evaluate left env, evaluate right env)) env
            | LessThanOrEqualTo(Number(l), Number(r)) -> Boolean(l <= r)
            | LessThanOrEqualTo(left, right) -> evaluate (LessThanOrEqualTo(evaluate left env, evaluate right env)) env

    module Statement =
        let rec evaluate (statement: Statement) (env: Environment) =
            match statement with
            | DoNothing -> env
            | Assign(name, expression) -> env |> Env.mergeValue name (Expression.evaluate expression env)
            | Sequence(statements) -> List.fold (fun e stmt -> evaluate stmt e) env statements
            | While(condition, body) ->
                let rec loop env =
                    match (Expression.evaluate condition env) with
                    | Boolean(false) -> env
                    | Boolean(true) -> loop (evaluate body env)
                    | _ -> failwithf "Failed to evaluate While loop: condition did not evaluate to Boolean (%O)" (Expression(condition) |> toString)
                loop env
                    
                        